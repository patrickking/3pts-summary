# Steno3d Shapefile Parser

A simple tool to transform a shapefile into a textured surface for Steno3d. 

_TODO: all of this file_


## Installing Prerequisites

To install GDAL, use homebrew: `brew install gdal`. Note that the latest version available on hombrew (at this writing) is 1.11.3, where the latest GDAL is at 2.1. 

To install the Python bindings for GDAL: `pip install --global-option=build_ext --global-option="-L/usr/local/include/gdal/" 'gdal==1.9.1'`. Note that this is an older version of the Python bindings, as the latest requires GDAL 2.1 and is not backwards compatible. 

Setup pythonpath:

on osx, using the kyngchaos version of qgis, http://www.kyngchaos.com/software/qgis, add this to ~/.bash_profile: `export PYTHONPATH="/Applications/QGIS.app/Contents/Resources/python"`

other platforms see here ... http://gis.stackexchange.com/questions/139818/setting-pythonpath























































