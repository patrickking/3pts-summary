import sys
import os
import qgis
import PyQt4

from qgis.core import *
from qgis.utils import *
from qgis.gui import *


from PyQt4.QtCore import *
from PyQt4.QtGui import *

# initialize QGIS

app = QgsApplication([], True)
QgsApplication.setPrefixPath('/Applications/QGIS.app/Contents/MacOS', True )
# userPath = os.path.expanduser('~')
# dataFilesDir = os.path.join(userPath, 'Data', 'Claremont')
# vectorLayerPath = os.path.join(dataFilesDir, 'contours.shp')

QgsApplication.initQgis()

# Add layer to instance
vlayer = QgsVectorLayer('data/CALGIS_CITY_AMENITY/CALGIS_CITY_AMENITY.shp', 'points', 'ogr')
vlayer.loadNamedStyle('data/CALGIS_CITY_AMENITY/CALGIS_CITY_AMENITY.qml')
QgsMapLayerRegistry.instance().addMapLayer(vlayer)

if vlayer.isValid():
  print "vlayer is valid."
  print 'feature count: %d' %(vlayer.featureCount())

# Adjust layer Settings
# Code sample from http://gis.stackexchange.com/questions/77870/how-to-label-vector-features-programmatically

# palyr = QgsPalLayerSettings() 
# palyr.readFromLayer(vlayer)
# palyr.enabled = True 
# palyr.fieldName = 'elev'
# # palyr.fontMinPixelSize = 20
# palyr.size = 20
# palyr.textColor = Qt.blue
# palyr.drawLabels = True
# palyr.placement= QgsPalLayerSettings.OverPoint 
# palyr.setDataDefinedProperty(QgsPalLayerSettings.Size,True,True,'8','') 
# palyr.writeToLayer(vlayer)

mapRenderer = QgsMapRenderer()

rect = vlayer.extent()
print rect.toString()
mapRenderer.setExtent(rect)
# mapRenderer.setLabelingEngine(QgsPalLabeling())
lst = [vlayer.id()]

mapRenderer.setLayerSet(lst)

c = QgsComposition(mapRenderer)
c.setPlotStyle(QgsComposition.Print)
# c.setPlotStyle(QgsComposition.Preview)
x, y = 300, 300
w, h = c.paperWidth(), c.paperHeight()
print 'width: %f' %(w)
print 'height: %f' %(h)
composerMap = QgsComposerMap(c,x,y,w,h)
# composerMap.drawCanvasItems()


print 'current extent width: %f,  height %f' \
    %(composerMap.currentMapExtent().width(), composerMap.currentMapExtent().height())

c.addItem(composerMap)
# c.addComposerMap(composerMap)
# composerLabel = QgsComposerLabel(c)


# composerLabel.adjustSizeToText()
# c.addItem(composerLabel)
# c.addComposerLabel(composerLabel)
# composerLabel.setItemPosition(20,10)
# composerLabel.setItemPosition(20,10, 100, 30)

# legend = QgsComposerLegend(c)
# legend.model().setLayerSet(mapRenderer.layerSet())
# c.addItem(legend)

#set image sizing
dpi = c.printResolution()
dpmm = dpi / 25.4
width = int(dpmm * c.paperWidth())
height = int(dpmm * c.paperHeight())
img = QImage(QSize(width, height), QImage.Format_ARGB32)
img.setDotsPerMeterX(dpmm * 1000)
img.setDotsPerMeterY(dpmm * 1000)
img.fill(Qt.transparent)
imagePainter = QPainter(img)
sourceArea = QRectF(0, 0, c.paperWidth(), c.paperHeight())
targetArea = QRectF(1000, 1000, width, height)

#renders image
print 'Rendering Image'
# c.refreshItems()
# c.updateBounds()

# composerMap.draw(imagePainter, sourceArea, QSize(300, 300), 96)

c.render(imagePainter, targetArea, sourceArea)
# c.renderPage(imagePainter, 0)
imagePainter.end()
img.save('qgis_test.png', 'png')
QgsApplication.exitQgis()