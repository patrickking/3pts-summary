from PyQt4.QtCore import QSize
from PyQt4.QtGui import QPainter, QImage, QColor
from PyQt4.QtCore import QFileInfo

from qgis.core import *
# import qgis.utils


def rasterize():


    QgsApplication.setPrefixPath('/Applications/QGIS.app/Contents/MacOS', True)
    # QgsApplication.setPrefixPath('/usr', True)
    QgsApplication.initQgis()
    app = QgsApplication([], True)



    # rlayer = QgsVectorLayer('data/CALGIS_STREET_NETWORK_MAJOR/CALGIS_STREET_NETWORK_MAJOR.shp', 'points', 'ogr')
    # rlayer.loadNamedStyle('data/CALGIS_STREET_NETWORK_MAJOR/CALGIS_STREET_NETWORK_MAJOR.qml')

    # rlayer = QgsVectorLayer('data/CALGIS_CITYBOUND_QUADRANT/CALGIS_CITYBOUND_QUADRANT.shp', 'points', 'ogr')
    # rlayer.loadNamedStyle('data/CALGIS_CITYBOUND_QUADRANT/CALGIS_CITYBOUND_QUADRANT.qml')


    # rlayer = QgsVectorLayer('data/CALGIS_CITY_AMENITY/CALGIS_CITY_AMENITY.shp', 'points', 'ogr')
    # rlayer.loadNamedStyle('data/CALGIS_CITY_AMENITY/CALGIS_CITY_AMENITY.qml')


    # rlayer = QgsVectorLayer('data/CALGIS_ADM_FIRE_STATION_OFFICE/CALGIS_ADM_FIRE_STATION_OFFICE.shp', 'points', 'ogr')
    # rlayer.loadNamedStyle('data/CALGIS_CITY_AMENITY/CALGIS_CITY_AMENITY.qml')


    rlayer = QgsVectorLayer('data/multipoint.shape/multipoint.shp', 'points', 'ogr')









    # lyr_id = lyr.id()
    # QgsMapLayerRegistry.instance().addMapLayer(rlayer)

    rlayer.triggerRepaint()

    # lyr.loadNamedStyle('test.qml')







    # fileInfo = QFileInfo(fileName)
    # baseName = fileInfo.baseName()
    # rlayer = QgsVectorLayer(fileName, baseName)
    # if not rlayer.isValid():
      # print "Layer failed to load!"




    # rlayer.setDrawingStyle('MultiBandColor') # make sure
    QgsMapLayerRegistry.instance().addMapLayer(rlayer)

    # create image
    # print(rlayer)
    # print(rlayer.width())
    # w = rlayer.width()
    # h = rlayer.height()
    img = QImage(QSize(5000, 5000), QImage.Format_ARGB32_Premultiplied)

    # set image's background color
    color = QColor(127, 127, 127)
    # color = QColor(255, 255, 255)
    img.fill(color.rgb())

    # create painter
    p = QPainter()
    p.begin(img)
    p.setRenderHint(QPainter.Antialiasing)

    render = QgsMapRenderer()
    render.setProjectionsEnabled(True)

    # set layer set
    lst = [rlayer.id()]  # add ID of every layer
    render.setLayerSet(lst)

    # set extent
    rect = rlayer.extent()
    # rect = QgsRectangle(render.fullExtent())
    rect.scale(1.0)

    render.setExtent(rect)

    # set output size
    render.setOutputSize(img.size(), img.logicalDpiX())

    # do the rendering
    render.render(p)

    p.end()

    # save image
    result = img.save('qgis_test.png', "png")
    # print(result)




    QgsApplication.exitQgis()







rasterize()


# try:
#     rasterize()
# except Exception as e:
#     print 'exception! '
#     print e
# except:













