import random
from osgeo import gdal, ogr

gdal.UseExceptions()


RASTERIZE_COLOR_FIELD = "__color__"



def rasterize(pixel_size=25):
    # Open the data source
    orig_data_source = ogr.Open('data/CALGIS_STREET_NETWORK_MAJOR/CALGIS_STREET_NETWORK_MAJOR.shp')
    # orig_data_source = ogr.Open('data/CALGIS_CITY_AMENITY/CALGIS_CITY_AMENITY.shp')
    # orig_data_source = ogr.Open('data/CALGIS_ATS_SECTIONS/CALGIS_ATS_SECTIONS.shp')
    # orig_data_source = ogr.Open('data/CALGIS_CITYBOUND_QUADRANT/CALGIS_CITYBOUND_QUADRANT.shp')

    # Make a copy of the layer's data source because we'll need to
    # modify its attributes table
    source_ds = ogr.GetDriverByName("Memory").CopyDataSource(
            orig_data_source, "")
    source_layer = source_ds.GetLayer(0)
    source_srs = source_layer.GetSpatialRef()
    x_min, x_max, y_min, y_max = source_layer.GetExtent()

    # Create a field in the source layer to hold the features colors
    field_def = ogr.FieldDefn(RASTERIZE_COLOR_FIELD, ogr.OFTReal)
    source_layer.CreateField(field_def)
    source_layer_def = source_layer.GetLayerDefn()
    field_index = source_layer_def.GetFieldIndex(RASTERIZE_COLOR_FIELD)

    # Generate random values for the color field (it's here that the value
    # of the attribute should be used, but you get the idea)
    for feature in source_layer:
        # feature.SetField(field_index, 255)
        feature.SetField(field_index, random.randint(0, 255))

        # Time to try some styling
        feature.SetStyleString('PEN(c:#FF0000,w:5px)')

        source_layer.SetFeature(feature)

    # Create the destination data source
    x_res = int((x_max - x_min) / pixel_size)
    y_res = int((y_max - y_min) / pixel_size)

    memory_dataset = gdal.GetDriverByName('MEM').Create('in mem', x_res,
      y_res, 3, gdal.GDT_Byte)

    memory_dataset.SetGeoTransform((
            x_min, pixel_size, 0,
            y_max, 0, -pixel_size,
        ))
    if source_srs:
        # Make the target raster have the same projection as the source
        memory_dataset.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        memory_dataset.SetProjection('LOCAL_CS["arbitrary"]')

    # Rasterize
    err = gdal.RasterizeLayer(memory_dataset, (3, 2, 1), source_layer,
            burn_values=(0, 0, 0),
            options=["ATTRIBUTE=%s" % RASTERIZE_COLOR_FIELD])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)

    # Out to PNG
    target_ds = gdal.GetDriverByName('PNG').CreateCopy('calgary.png', memory_dataset)





def inspect():

    # shp = ogr.Open('data/CALGIS_CITYBOUND_QUADRANT/CALGIS_CITYBOUND_QUADRANT.shp')

    shp = ogr.Open('data/CALGIS_CITY_AMENITY/CALGIS_CITY_AMENITY.shp')


    lyr = shp.GetLayer(0)
    for i in range(0, 4):
        feat = lyr.GetFeature(i)
        geom = feat.GetGeometryRef()
        print geom.GetGeometryRef(0)


# rasterize()
# inspect()

