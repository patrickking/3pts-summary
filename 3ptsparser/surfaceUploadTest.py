import numpy as np
import steno3d
import json
import os
steno3d.login(endpoint='https://steno3d-staging.appspot.com/')

import gdal

# open dataset
# ds = gdal.Open('/Users/stephaniesachrajda/3ptsparser/Sample Data/cea.tif')

print ds.GetRasterBand(1)
assets = '/Users/stephaniesachrajda/3ptscience/steno3dpy/assets/'
with open(assets + '/teapot.json','r') as f:
    data = json.loads(f.read())

S = steno3d.resources.Surface(
    mesh=dict(
        triangles=data['triangles'],
        vertices=data['vertices'],
    ),
)
print(S.upload())
# P = steno3d.project.Project(resources=[S])
# print(P.upload())