import steno3d
steno3d.login(endpoint='https://steno3d-staging.appspot.com/')

proj = steno3d.Project(title='Surface Project')

from steno3d.examples import Teapot


import numpy as np
assert np.max(Teapot.triangles) < len(Teapot.vertices)
assert np.min(Teapot.triangles) >= 0

print('vertices: size = ' + str(Teapot.vertices.shape)
      + ', type = ' + str(Teapot.vertices.dtype))
print('triangles: size = ' + str(Teapot.triangles.shape)
      + ', type = ' + str(Teapot.triangles.dtype))


my_mesh_2D = steno3d.Mesh2D(
    vertices=Teapot.vertices,
    triangles=Teapot.triangles,
    opts=dict(
        wireframe=False
    )
)


my_mesh_2D_grid = steno3d.Mesh2DGrid(
    h1=np.ones(10),
    h2=np.ones(10),
    x0=[0,0,0],
    Z=np.random.rand(121)
)


my_surface_resource = steno3d.Surface(
    project=proj,
    mesh=my_mesh_2D,
    title='My Teapot Surface',
    description="I'm a little teapot short and stout."
)


my_surface_resource.validate()


my_surface_resource.opts = dict(
    color=[100, 100, 0],
    opacity=1
)



random_face_data = steno3d.DataArray(
    array=np.random.rand(len(my_mesh_2D.triangles)),
    title='Random Face Data'
)


vertex_z_data = steno3d.DataArray(
    array=my_mesh_2D.vertices[:,2],
    title='Z-Coord Values'
)


# Make sure the data we created is the right length.
# This is also checked in validation.
assert len(vertex_z_data.array) == len(my_mesh_2D.vertices)
assert len(random_face_data.array) == len(my_mesh_2D.triangles)

# Bind the data
my_surface_resource.data = [
    dict(
        location='N',
        data=vertex_z_data
    ),
    dict(
        location='CC',
        data=random_face_data
    )
]





































































