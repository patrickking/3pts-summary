import numpy as np
import os
import json
import steno3d

from random import random
from random import uniform

steno3d.login(endpoint="https://steno3d-staging.appspot.com/",
              devel_key="patrickking//14e58e7b-f1b5-4577-bb2a-5408170b6c49")


my_project = steno3d.Project()
my_project.title = 'Stephs Excellent Project'
my_project.public = True


# point part

mesh = steno3d.Mesh0D()
mesh.vertices = [
  [0.0, 0.0, 0.0],
  [0.0, 1.0, 0.0],
  [0.0, 0.0, 1.0],
  [1.0, 0.0, 0.0],
  [1.0, 1.0, 1.0],
  [0.5, 0.5, 0.5],
  [0.55, 0.55, 0.55],
  [0.45, 0.45, 0.45],
]

texture = steno3d.Texture2DImage(O=[0, 0, 0],
                                 U=[1, 0, 0],
                                 V=[0, 1, 0],
                                 image='metal.png')

point = steno3d.Point(project=my_project,
                      mesh=mesh,
                      textures=[texture],)

# line part

# line_mesh = steno3d.Mesh1D()
# line_mesh.vertices = [
#   [0.1, 0.1, 0.1],
#   # [0.6, 0.5, 0.5],
#   # [0.6, 0.6, 0.5],
#   # [0.6, 0.6, 0.6],
#   # [0.6, 0.6, 0.7],
#   # [0.6, 0.7, 0.7],
#   # [0.8, 0.7, 0.7],
#   # [0.8, 0.8, 0.7],
#   # [0.8, 0.8, 0.8],
#   # [0.8, 0.8, 0.9],
#   # [0.8, 0.9, 0.9],
#   [0.9, 0.9, 0.9],
# ]

# line_mesh.segments = [
#   [0, 1],
#   # [1, 2],
#   # [2, 3],
#   # [3, 4],
#   # [4, 5],
#   # [5, 6],
#   # [6, 7],
#   # [7, 8],
#   # [8, 9],
#   # [9, 10],
#   # [10, 11],
# ]

# point = steno3d.Line(project=my_project,
#                      mesh=line_mesh,)

# plane part


# surface_mesh = steno3d.Mesh2D()

# surface_mesh.vertices = [
#   [0.8, 0.4, 0.0],
#   [0.8, -0.8, 0.0],
#   [-0.8, -0.8, 0.0],
#   [-0.8, 0.8, 0.0],
#   [0.0, 0.0, 0.0],
# ]

# surface_mesh.triangles = [
#   [0, 1, 4],
#   [1, 2, 4],
#   [2, 3, 4],
#   [3, 0, 4],
# ]

# surface = steno3d.Surface(project=my_project,
#                           mesh=surface_mesh)


# make random triangles!
surface_mesh = steno3d.Mesh2D()

verts = [[0.0, 0.0, 0.0]]

tris = []

for i in range(25):
  print(i)

  verts.append([uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)])
  verts.append([uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)])

  tris.append([0, i*2 + 1, i*2 + 2])


surface_mesh.vertices = verts
surface_mesh.triangles = tris
surface = steno3d.Surface(project=my_project,
                          mesh=surface_mesh)

# volume part

volume_mesh = steno3d.Mesh3DGrid()

volume_mesh.h1 = [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, ]
volume_mesh.h2 = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, ]
volume_mesh.h3 = [0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, ]


volume = steno3d.Volume(project=my_project,
                        mesh=volume_mesh)














my_project.upload()
