import steno3d
from random import uniform

steno3d.login(endpoint="https://steno3d-staging.appspot.com/")

my_project = steno3d.Project(title='WebGL Context Bug')


# point part

mesh = steno3d.Mesh0D()
mesh.vertices = [
  [0.0, 0.0, 0.0],
  [0.0, 1.0, 0.0],
  [0.0, 0.0, 1.0],
  [1.0, 0.0, 0.0],
  [1.0, 1.0, 1.0],
  [0.5, 0.5, 0.5],
  [0.55, 0.55, 0.55],
  [0.45, 0.45, 0.45],
]

texture = steno3d.Texture2DImage(O=[0, 0, 0],
                                 U=[1, 0, 0],
                                 V=[0, 1, 0],
                                 image='metal.png')

point = steno3d.Point(project=my_project,
                      mesh=mesh,
                      textures=[texture],)


# Make 50 surfaces, each with a mesh consisting of a single triangle
for i in range(50):
    surface_mesh = steno3d.Mesh2D()

    surface_mesh.vertices = [
      [0.0, 0.0, 0.0],
      [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)],
      [uniform(-1.0, 1.0), uniform(-1.0, 1.0), uniform(-1.0, 1.0)],
    ]

    surface_mesh.triangles = [[0, 1, 2]]

    surface = steno3d.Surface(project=my_project,
                              mesh=surface_mesh)


my_project.upload()
