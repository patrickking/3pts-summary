import steno3d

steno3d.login(endpoint="https://steno3d-staging.appspot.com/")

my_project = steno3d.Project(title='Multiple Upload Issue')

mesh = steno3d.Mesh0D()
mesh.vertices = [[0.0, 0.0, 0.0]]

point = steno3d.Point(project=my_project, mesh=mesh)

# This upload works fine
my_project.upload()

texture = steno3d.Texture2DImage(O=[0, 0, 0],
                                 U=[1, 0, 0],
                                 V=[0, 1, 0],
                                 image='metal.png')
point.textures = [texture]

# This upload leads to an exception.
my_project.upload()
