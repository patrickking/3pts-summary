import numpy as np
import os
import json
import steno3d


def uploadProject(topography='./Sample Data/topography.json',
                  img='./woodplanks.png'):
    with open(topography, 'r') as f:
        data = json.loads(f.read())

    project = steno3d.Project()
    project.title = "Abhi's Excellent Project"
    project.public = True

    M = steno3d.Mesh2D(triangles=data['triangles'], vertices=data['vertices'])
    # I = steno3d.resources.texture.Image2D(O=[443200, 491750, 0], U=[4425, 0, 0], V=[0, 3690, 0], image=img)
    I = steno3d.Texture2DImage(O=[0, 0, 0],
                               U=[1, 0, 0],
                               V=[0, 1, 0],
                               image=img)
    D = steno3d.DataArray(array=np.array(data['vertices'])[:, 1],
                          title='Elevation')
    S = steno3d.Surface(project=project,
                        mesh=M,
                        textures=[I],
                        data=[dict(location='N', data=D)])

    return project.upload()

steno3d.login(endpoint='https://steno3d-staging.appspot.com/')

print uploadProject(topography='./Sample Data/simpleTopo.json')
