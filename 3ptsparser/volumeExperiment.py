import steno3d


import numpy as np
padding = np.array(range(10))
uniform = np.ones(20)
x_spacing = np.hstack((np.flipud(padding), uniform, padding))
y_spacing = x_spacing
z_spacing = np.hstack((np.flipud(padding), uniform))


steno3d.login(endpoint="https://steno3d-staging.appspot.com/")

my_project = steno3d.Project(title='Experimenting with Volumes')

volume_mesh = steno3d.Mesh3DGrid()

volume_mesh.h1 = x_spacing
volume_mesh.h2 = y_spacing
volume_mesh.h3 = z_spacing

# volume_mesh.h1 = [1.0, 2.0, 3.0, 4.0, 5.0]
# volume_mesh.h2 = [1.0, 2.0, 3.0, 4.0, 5.0]
# volume_mesh.h3 = [1.0, 2.0, 3.0, 4.0, 5.0]


# texture = steno3d.Texture2DImage(O=[0, 0, 0],
#                                  U=[1, 0, 0],
#                                  V=[0, 1, 0],
#                                  image='metal.png')

volume = steno3d.Volume(project=my_project,
                        mesh=volume_mesh)

my_project.upload()
volume.plot()


